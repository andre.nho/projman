# projman

Project manager console tool for sole developers, with focus in game development.

Features:
 - project design (?)
 - backlog list
   - organized in form of tree
   - each item can have:
      - subitems
      - dependence
      - priority
      - category
      - points
      - to be done / done
   - actions
      - add at the end
      - add child
      - add dependence
      - add to current sprint
      - move up/down
      - change priority
      - change category
      - mark as done
      - show undone
 - sprints
   - create/delete
   - mark as current
   - edit task
   - mark task as done
 - bug list
 - admin
   - category management

Backlog view is same as sprint view, only more limited.
