# Pending tasks

## Sprint 0 (current)

- [X] Project skeleton 
  - [X] Add license
  - [X] Create autotools basic files
  - [X] Add curses
  - [X] Add memory check
  - [X] Create test infrastructure
- [ ] Define file format / struct 
  - [ ] Define file format  (data)  <!-- abcd1234 -->
  - [ ] Define struct  (data, code)  <!-- 1fj3abcd -->

## Sprint 1

- [ ] Library
  - [ ] Read TASKS file
    - [ ] Write tests
  - [ ] Write TASKS file
    - [ ] Write tests
  - [ ] Sprints

## Sprint 2

- [ ] Main view (backlog/sprint)
  - [ ] Initialize curses
  - [ ] Create main, configurable view
    - [ ] Add items (sibling/child)
    - [ ] Change category
    - [ ] Mark as done

## Sprint 3

- [ ] Main view (backlog/sprint)
  - [ ] Create dependency
  - [ ] Sprint management
    - [ ] Create / edit / delete
    - [ ] Mark as current
  - [ ] Move items between backlog and sprint
    - [ ] Open in current
  - [ ] All tasks

## Sprint 4

- [ ] Configuration file / commandline options
  - [ ] Filename
  - [ ] Category list

## Backlog

-----------------------------------------------------------------

# Completed tasks

