#include "task.hh"

#include <algorithm>
#include <iostream>
#include <sstream>
#include <random>
#include <regex>
#include <stack>
#include <utility>
using namespace std;

static vector<string>
parse_tags(string s)
{
    static regex regex_tag(R"([^,\s]+)");

    vector<string> tags;
    smatch m;
    while (regex_search(s, m, regex_tag)) {
        tags.push_back(m[0].str());
        s = m.suffix().str();
    }

    return tags;
}

string
create_task_id(string const& s)
{
    if (!s.empty())
        return s;

    static random_device r;
    static mt19937 gen(r());
    static uniform_int_distribution<> dis(0, 10 + 26 + 26 - 1);

    string str(8, ' ');
    for (size_t i=0; i < 8; ++i) {
        int n = dis(gen);
        if (n < 10)
            str[i] = '0' + n;
        else if (n < 10 + 26)
            str[i] = 'A' + n - 10;
        else
            str[i] = 'a' + n - 10 - 26;
    }

    return str;
}

vector<Task>  
load_tasks(string const& text)
{
    vector<Task> tasks;

    // header format: ## Backlog / Sprint N [(current)]
    static regex regex_header(R"(^##\s+(Backlog|Sprint\s+(\d+))(\s+\(current\))?)");
    // line format: [spaces]* - [ X? ] description [([tags]+)] [<!-- ID -->]
    static regex regex_item(R"(^(\s*)-\s?\[([X\s])\]\s*(.*?)\s*(?:\((.*?)\))?\s*(?:<!--\s*([A-Za-z0-9]+)\s*-->)?\s*$)");  // TODO - id

    struct Context {
        optional<int>             sprint;
        vector<pair<string, int>> parent;
    } ctx;

    istringstream in(text);
    string line;
    while (getline(in, line)) {
        smatch m; 

        // check header - TODO

        // check item
        if (regex_search(line, m, regex_item)) {
            int            spaces = m[1].str().size();
            bool           done = (m[2].str() == "X");
            string         description = m[3].str();
            vector<string> tags = parse_tags(m[4].str());
            string         id = create_task_id(m[5].str());

            optional<string> parent;
            while (!ctx.parent.empty() && ctx.parent.back().second > spaces)
                ctx.parent.pop_back();
            if (!ctx.parent.empty())
                parent = ctx.parent.back().first;

            tasks.push_back({ 
                id, description, done, 
                parent,
                {}, // TODO - children id
                tags,
                ctx.sprint,
            });

            if (!ctx.parent.empty())
                ctx.parent.pop_back();
            ctx.parent.push_back({ id, spaces });
        }   

        // otherwise, discard input
    }

    return tasks;
}

string
write_tasks(vector<Task> const& task)
{
    return "";
}

// vim: ts=4:sw=4:sts=4:expandtab:foldmethod=marker
