#ifndef TASK_HH_
#define TASK_HH_

#include <optional>
#include <string>
#include <vector>
using namespace std;

struct Task {
    string           id;
    string           description;
    bool             done = false;
    optional<string> parent_id;
    vector<string>   children_id;
    vector<string>   tags;
    optional<int>    sprint;
};

vector<Task> load_tasks(string const& text);
string       write_tasks(vector<Task> const& task);

#endif

// vim: ts=4:sw=4:sts=4:expandtab:foldmethod=marker
