#define DOCTEST_CONFIG_IMPLEMENT_WITH_MAIN
// #define DOCTEST_CONFIG_SUPER_FAST_ASSERTS
#include "doctest.h"

#include "task.hh"

TEST_CASE("Single item import") {  // {{{

    SUBCASE("ID generation") {
        extern string create_task_id(string const& s);
        string id1 = create_task_id("");
        string id2 = create_task_id("");
        string id3 = create_task_id("");
        INFO("Three ID examples: " << id1 << " " << id2 << " " << id3);
        CHECK(id1.size() >= 8);
        for (auto c : id1)
            CHECK(((c >= '0' && c < '9') || (c >= 'A' && c <= 'Z') || (c >= 'a' && c <= 'z')));
    }
    
    SUBCASE("Simple on-liner") {
        auto tasks = load_tasks("- [X] This is a test ");
        REQUIRE(tasks.size() == 1);
        CHECK(tasks[0].id.size() >= 8);  // auto-created ID
        CHECK(tasks[0].description == "This is a test");
        CHECK(tasks[0].done);
        CHECK(!tasks[0].parent_id);
        CHECK(tasks[0].children_id.empty());
        CHECK(tasks[0].tags.empty());
        CHECK(!tasks[0].sprint);
    }

    SUBCASE("Tags") {
        auto tasks = load_tasks("- [X] This is a test  (prog, docs) ");
        REQUIRE(tasks.size() == 1);
        CHECK(tasks[0].description == "This is a test");
        REQUIRE(tasks[0].tags.size() == 2);
        CHECK(tasks[0].tags[0] == "prog");
        CHECK(tasks[0].tags[1] == "docs");
    }

    SUBCASE("ID") {
        auto tasks = load_tasks("- [X] This is a test  (prog, docs)  <!-- abcd1234 --> ");
        REQUIRE(tasks.size() == 1);
        CHECK(tasks[0].description == "This is a test");
        CHECK(tasks[0].id == "abcd1234");
        REQUIRE(tasks[0].tags.size() == 2);
        CHECK(tasks[0].tags[0] == "prog");
        CHECK(tasks[0].tags[1] == "docs");
    }

    SUBCASE("ID without tag") {
        auto tasks = load_tasks("- [X] This is a test  <!-- abcd1234 --> ");
        REQUIRE(tasks.size() == 1);
        CHECK(tasks[0].description == "This is a test");
        CHECK(tasks[0].id == "abcd1234");
        CHECK(tasks[0].tags.empty());
    }
}  // }}}

TEST_CASE("Multiple item import") {  // {{{

    SUBCASE("Children") {
        auto tasks = load_tasks(R"(
- [ ] Parent
  - [ ] Child 1
  - [ ] Child 2
        )");
        REQUIRE(tasks.size() == 3);
        CHECK(tasks[1].parent_id == tasks[0].id);
        CHECK(tasks[2].parent_id == tasks[0].id);
        REQUIRE(tasks[0].children_id.size() == 2);
        CHECK((tasks[0].children_id[0] == tasks[1].id || tasks[0].children_id[0] == tasks[2].id));
        CHECK((tasks[0].children_id[1] == tasks[1].id || tasks[0].children_id[1] == tasks[2].id));
        CHECK(tasks[0].children_id[0] != tasks[0].children_id[1]);
        CHECK(tasks[0].id != tasks[1].id);
        CHECK(tasks[1].id != tasks[2].id);
    }

    SUBCASE("Children") {
        auto tasks = load_tasks(R"(
- [ ] Parent 1
  - [ ] Child 1
- [ ] Parent 2
        )");
        REQUIRE(tasks.size() == 3);
        CHECK(!tasks[0].parent_id);
        CHECK(tasks[1].parent_id == tasks[0].id);
        CHECK(!tasks[2].parent_id);
        REQUIRE(tasks[0].children_id.size() == 1);
        CHECK(tasks[0].children_id[0] == tasks[1].id);
    }

    SUBCASE("Repeated ID") {
        auto tasks = load_tasks(R"(
- [ ] Parent 1 <!-- abcd1234 -->
- [ ] Parent 2 <!-- abcd1234 -->
  - [ ] Child 1
        )");
        REQUIRE(tasks.size() == 2);
        CHECK(tasks[0].id == "abcd1234");
        CHECK(tasks[1].id != "abcd1234");
        CHECK(tasks[0].description == "Parent 1");
        CHECK(tasks[1].description == "Child 1");
        REQUIRE(tasks[0].children_id.size() == 1);
        CHECK(tasks[0].children_id[0] == tasks[1].id);
        CHECK(tasks[1].parent_id == tasks[0].id);
    }

    SUBCASE("Subchildren") {
        auto tasks = load_tasks(R"(
- [ ] Parent 1
  - [ ] Child 1
    - [ ] Subchild
  - [ ] Child 2
- [ ] Parent 2
        )");
        REQUIRE(tasks.size() == 5);
        CHECK(!tasks[0].parent_id);
        CHECK(tasks[1].parent_id == tasks[0].id);
        CHECK(tasks[2].parent_id == tasks[1].id);
        CHECK(tasks[3].parent_id == tasks[0].id);
        CHECK(tasks[4].parent_id);
        REQUIRE(tasks[0].children_id.size() == 2);
        REQUIRE(tasks[1].children_id.size() == 1);
        REQUIRE(tasks[2].children_id.empty());
    }

    
}  // }}}

// vim: ts=4:sw=4:sts=4:expandtab:foldmethod=marker
